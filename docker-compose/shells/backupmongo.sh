#! /bin/sh
cur_date="`date +%Y_%m_%d_%H_%M`"
if [ ! -d "backup/mongo" ]; then
  mkdir -p backup/mongo
fi
mongodump -u admin -p admin --authenticationDatabase admin -o backup/mongo/dump  \
&& tar -zcvf backup/$cur_date.mongo.tar.gz backup/mongo/dump/ \
# && rm -rf backup/mongo/dump