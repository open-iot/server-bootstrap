#!/bin/bash

# change work dir
REAL_PATH=
BOOTSTRAP_DIR=$(dirname $(realpath $0))
cd $BOOTSTRAP_DIR

# cached credentials, timeout 5min
sudo uname -ra

source scripts/function.sh

export BOOTSTRAP_DIR
export PROJECT_ROOTDIR=$(realpath $BOOTSTRAP_DIR/../projects)

echo "
PRODUCT_PROFILE:${PRODUCT_PROFILE}
BOOTSTRAP_DIR:$BOOTSTRAP_DIR
"

# usage: RUN_MODE=s $0 key-word
FILTER=${1:-".sh$"}
RUN_MODE=${RUN_MODE:-"f"}  # s=single mode,f=full
DEPLOY_DIR="scripts/deploy"
STATE="ONE-STEP"

echo "
FILTER=$FILTER
RUN_MODE=${RUN_MODE}
DEPLOY_DIR=$DEPLOY_DIR
STATE=$STATE
"

for STEP in `ls $DEPLOY_DIR | grep -v ".disable$"`
do
    if [ x$RUN_MODE == x"s" ] ; then
        BREAK=TRUE
    fi

    CUR_STEP=$DEPLOY_DIR/$STEP
    if [ x"$STATE" != x"CONTINUE" ] && [ -z "$(echo $CUR_STEP | grep "$FILTER")" ] ; then
        echo skip $CUR_STEP
    else
        echo "--------------------------------------"
        echo "start $CUR_STEP"

        bash $CUR_STEP
        RET=$?
        if [ $RET -eq 1 ]; then
            source ${PRODUCT_PROFILE}
        fi
        # check interrupt flag
        if [ $RET -eq 2 ]; then
            echo "break proactive"
            BREAK=TRUE
        fi

        echo "done $CUR_STEP"
        sleep 1

        # 'single mode' will be break the loop
        if [ x$BREAK == x"TRUE" ] ; then
            echo "break loop"
            break
        else
            STATE="CONTINUE"
        fi
    fi
done

PATH=$(unrepeat_path)
