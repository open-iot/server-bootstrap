#!/bin/bash

unalias cp
RUN_MODE=s

BOOTSTRAP_DIR=$(dirname $(realpath $0))
cd $BOOTSTRAP_DIR

LIST="
01-prepare-node-env.sh
02-prepare-docker-env.CentOS.sh
20-install-docker-compose.sh
31-cleanup-docker-container.sh
33-cleanup-pm2-instance.sh
50-start-containers-V2.0.0.sh
60-start-rich-backend.sh
60-start-rich-frontend.sh
61-start-xixun-RealTimeServer.sh
"

for script in $LIST
do
    source ./bootstrap.sh $script
done

pm2 startup
pm2 save --force

echo "all done"