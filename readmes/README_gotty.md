# running gotty inside docker container
docker run -p 8080:8080 --name web-tty -d dit4c/gotty

# run gotty in host
## share termial with a url
```
gotty --reconnect --reconnect-time "2" --random-url tmux new -A -s gotty # start tmux session to share
gotty --reconnect --reconnect-time "2" --random-url tmux new -A -s gotty # start tmux session to share with write access
tmux attach -t gotty # attach to the termimal type something what you want to
browser http://some-ip:port/random-url # will be to see something what the previous typing
```

# start interaction shell
```
gotty --permit-write --reconnect /bin/sh
nohup ./gotty -w --reconnect --reconnect-time 2 /bin/bash # start with nohup
```
