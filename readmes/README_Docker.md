# what is the docker
Docker is a containerized platform that allows you to quickly build, test, 
and deploy applications as a portable, self-contained container that can 
run almost anywhere. the following steps introduce docker related command
under Debian9, there are two terms need to known, they are image and container
image is a snapshot of docker container, we can get it from docker hub.
container is an instance of image, a container represents the runtime of
a single application, process, or service.

# install docker
```
apt install docker-ce
```

# start docker
```
systemctl start docker
```

# check docker status
```
systemctl status docker
```

# can be used by $USER
```
usermod -a -G docker $USER
```

# search docker image in docker hub
```
docker search debian
```

# pull image
```
docker image pull debian
```

# list images in local system
```
docker image ls
```

# remove docker image
```
docker image rm debian
```

# run container
```
docker container run debian # Stop immediately after starting, due to without a process loop to run caused quit soon, If do not have a local image, it will be downloaded first
docker container run -it debian /bin/bash # start a container with interaction mode
```

# list activity container
```
docker container ls
```

# list all container whatever it's activity or not
```
docker container ls -a
```

# remove a container
```
docker container rm container-id
```
