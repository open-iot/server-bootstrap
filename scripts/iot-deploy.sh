#!/bin/bash

# issues
# 1. fpm-mqtt-iotmiddleware restarting
# 2. fpm-schedule-server exited
# 3. 缺少fpm-cms-lld，fpm-smart-home-server，led-upload-page，webhook.yunplus.io

# requirements
# 显示屏客户需要全屏显示传感器数据，还有调整字体大小

# basic
apt-get update
apt install -y apt-transport-https ca-certificates curl gnupg2 lsb-release software-properties-common

# install docker
curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/debian/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/debian $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce

# install nodejs
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs

# install docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose  
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

# backend and middleware
git clone root@47.101.70.189:~/deploy-ruichen-docker
cd deploy-ruichen-docker
docker-compose up -d

# frontend
git clone root@47.101.70.189:~/openiot.yunplus.io
scp root@47.101.70.189:~/openiot.yunplus.io/config.json ~/openiot.yunplus.io/config.json
npm run build
npm run docker
npm run init:influx
npm install
npm run dev
ctrl+c
npm run run:pm2 # export NODE_ENV=development DEBUG=openiot.yunplus.io:*
