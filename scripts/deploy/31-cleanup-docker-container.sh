#!/bin/bash

DOCKER_AQ=`sudo docker ps -aq`

# keep docker containers clean
[ -z "${DOCKER_AQ}" ] || sudo docker rm -f ${DOCKER_AQ}
