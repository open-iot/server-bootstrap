#!/bin/bash

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt update
sudo apt-cache policy docker-ce

sudo apt install -y docker-ce git tig nginx

# can remove attached groud with command 'sudo gpasswd -d ${USER} docker'
sudo usermod -aG docker ${USER}

# su - ${USER}, relogin with `bash -l`
echo "
The docker installation is complete, and the current ${USER} has been added to the docker group, which can be
Execute docker directly the next time you log in, root privileges are required to run docker commands
In the current login session.
"
sleep 3 # hold 3s to show tips
