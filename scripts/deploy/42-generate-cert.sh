#!/bin/bash

COMPANY_NAME="ruichen"
DOMAIN="127.0.0.1"
EMAILADDRESS="support@yunplus.io"

# option
SUBJECT="/C=CN/ST=JiangSu/L=YZ/O=$COMPANY_NAME/OU=$COMPANY_NAME/CN=$DOMAIN/emailAddress=$EMAILADDRESS"

# generate private key with rsa alg
openssl genrsa -out privatekey.pem 1024

# generate a self-signed CA certificate
openssl req -new -key privatekey.pem -out certrequest.csr -subj $SUBJECT

# generatre x509 certificate
openssl x509 -req -in certrequest.csr -signkey privatekey.pem -days 5480 -out certificate.pem

# privatekey.pem # private key
# certrequest.csr # self-signed CA certificate
# certificate.pem # x509 certificate

# install to ssl directory
mv -v privatekey.pem certrequest.csr certificate.pem $PROJECT_ROOTDIR/deploy-ruichen-docker/ssl
