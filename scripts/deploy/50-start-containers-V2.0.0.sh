#!/bin/bash

DOCKER_COMPOSE=$HOME/bin/docker-compose
DOCKER_WORKDIR=$BOOTSTRAP_DIR/docker-compose

sudo ${DOCKER_COMPOSE} -f $DOCKER_WORKDIR/docker-compose.yml up -d

# work-around
sudo ${DOCKER_COMPOSE} -f $DOCKER_WORKDIR/docker-compose.yml down
# change access permission, TODO: is it necessary?
sudo chmod 777 -R $DOCKER_WORKDIR/emqx
sudo ${DOCKER_COMPOSE} -f $DOCKER_WORKDIR/docker-compose.yml up -d

# init emqx
$DOCKER_WORKDIR/emqx/init.sh

# check emqx work well or not
# TODO: the second time will be return exist, should know why it behaves this way; may be some delay is necessary to wait emqx server ready.
echo_exec sleep 15
$DOCKER_WORKDIR/emqx/init.sh # {"code":0}, should be ok
$DOCKER_WORKDIR/emqx/init.sh # {"message":"existed"}
