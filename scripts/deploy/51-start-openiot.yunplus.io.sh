#!/bin/bash

cd $PROJECT_ROOTDIR/openiot.yunplus.io

# start depnendence services
#npm run docker
# temporary solution: binary installed in $USER/bin directory, can not found by root user
DOCKER_COMPOSE=$HOME/bin/docker-compose
sudo ${DOCKER_COMPOSE} -f docker-compose.dev.yml up --build -d

# influx
npm run init:influx

# install node_module dependence
npm install

# build frontend
npm run build

# `npm run dev` to run in dev mode, release will be manager by pm2
# export NODE_ENV=development DEBUG=openiot.yunplus.io:*
# TODO: sometime can not work
# npm run run:pm2
NODE_ENV=development DEBUG=openiot.yunplus.io:* pm2 start source/app.js --name openiot.yunplus.io -i 1
