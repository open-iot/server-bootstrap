#!/bin/bash

mkdir -p $HOME/bin
tar xvf pkgs/node-v12.18.3-linux-x64.tar.xz -C $HOME/bin

# can use nvm to manager node version, preform `npm config delete prefix` when tips of conflict
# tar xvf pkgs/nvm.tar.gz -C $HOME/bin

# write env value to our product profile
cat << EOF > ${PRODUCT_PROFILE}
PATH=\$HOME/bin:\$PATH

# node env
unset i
unset -f pathmunge
NODE_HOME=\$HOME/bin/node-v12.18.3-linux-x64
NODE_PATH=\$NODE_HOME/lib/node_modules
PATH=\$NODE_HOME/bin:\$PATH
export NODE_HOME NODE_PATH PATH

[ -f \$HOME/bin/nvm/nvm.sh ] && source \$HOME/bin/nvm/nvm.sh
EOF

# install product.sh into inside of /etc/profile.d/ directory
sudo CUR_HOME=$HOME PRODUCT_PROFILE=${PRODUCT_PROFILE} sh -c 'echo "
# node env
export PRODUCT_PROFILE=${PRODUCT_PROFILE}
[ -f \${PRODUCT_PROFILE} ] && source \${PRODUCT_PROFILE}
" > /etc/profile.d/product.sh'

# install yarn pm2
echo "install yarn pm2 starting"
source ${PRODUCT_PROFILE}
npm install -g yarn pm2

exit 1
