#!/bin/bash

install_docker_compose()
{
    local VERSION="1.22.0"
    local VERSION="1.18.0" # production version
    local URL="https://github.com/docker/compose/releases/download/$VERSION/docker-compose-`uname -s`-`uname -m`"

    # there is an offline version in directory "pkgs"
    local ONLINE=N
    if [ x"$ONLINE" == xY ];
    then
        curl -L $URL -o /tmp/docker-compose
        sudo mv /tmp/docker-compose /usr/local/bin/docker-compose
    else
        mkdir -p $HOME/bin
        cp pkgs/docker-compose-Linux-x86_64-$VERSION $HOME/bin/docker-compose
    fi
}

install_docker_compose
