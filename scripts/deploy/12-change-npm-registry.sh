#!/bin/bash

assert_alicloud "due to aliyun host, skip setting .npmrc"

tee ~/.npmrc <<-'EOF'
registry = https://registry.npm.taobao.org
EOF
