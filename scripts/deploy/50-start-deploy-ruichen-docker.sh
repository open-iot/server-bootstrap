#!/bin/bash

DOCKER_COMPOSE=$HOME/bin/docker-compose

sudo ${DOCKER_COMPOSE} -f $PROJECT_ROOTDIR/deploy-ruichen-docker/docker-compose.yml up -d

# work-around
sudo ${DOCKER_COMPOSE} -f $PROJECT_ROOTDIR/deploy-ruichen-docker/docker-compose.yml down
# change access permission, TODO: is it necessary?
sudo chmod 777 -R $PROJECT_ROOTDIR/deploy-ruichen-docker/emqx
sudo ${DOCKER_COMPOSE} -f $PROJECT_ROOTDIR/deploy-ruichen-docker/docker-compose.yml up -d

# init emqx
$PROJECT_ROOTDIR/deploy-ruichen-docker/emqx/init.sh

# check emqx work well or not
# TODO: the second time will be return exist, should know why it behaves this way; may be some delay is necessary to wait emqx server ready.
echo_exec sleep 15
$PROJECT_ROOTDIR/deploy-ruichen-docker/emqx/init.sh # {"code":0}, should be ok
$PROJECT_ROOTDIR/deploy-ruichen-docker/emqx/init.sh # {"message":"existed"}
