#!/bin/bash

sudo apt update -y
sudo apt upgrade -y
sudo apt install -y g++ unzip tree
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common gnupg2
