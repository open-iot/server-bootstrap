#!/bin/bash

tar xvf pkgs/phantomjs-2.1.1-linux-x86_64.tar.bz2 -C $HOME/bin

if grep phantomjs ${PRODUCT_PROFILE} 2>&1 > /dev/null; then
    echo node env installed
else
    cat <<EOF >> ${PRODUCT_PROFILE}

# phantomjs
export PATH=\$PATH:\$HOME/bin/phantomjs-2.1.1-linux-x86_64/bin
EOF
fi

exit 1
