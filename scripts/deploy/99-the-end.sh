#!/bin/bash

SERVER_HOSTNAME="127.0.0.1"
is_alicloud_server
[ $? -eq 0 ] && SERVER_HOSTNAME=`sh scripts/get_public_ip.sh`

# summary 
echo "
port list here:
- 5080 yfsoftcom/try-influx
- 6379 redis-server
- 5001 fpm-mqtt-iotmiddleware
- 18443 fpm-mqtt-iotmiddleware
- 10009 fpm-mqtt-iotmiddleware
- 2080 filebrowser/filebrowser
- 18086 influxdb
- 1883 emqx/fpm-mqtt-server
- 11883 emqx/fpm-mqtt-server
- 7080 emqx/fpm-mqtt-server
- 7883 emqx/fpm-mqtt-server
- 7083 emqx/fpm-mqtt-server
- 10099 fpm-schedule-server
- 443 deploy-ruichen-docker_nginx_1
- 8099 deploy-ruichen-docker_nginx_1
- 8080 deploy-ruichen-docker_nginx_1
- 5000 registry
- 13000 grafana/grafana
- 18080 nginx-rtmp
- 11935 nginx-rtmp
- 1088 openiotyunplusio_phpmyadmin_1
- 13306 mysqlserver
- 9007 openiotyunplusio
- 10010 phpredisadmin

web pages:
    main: http://$SERVER_HOSTNAME:9007
    admin: http://$SERVER_HOSTNAME:9007/admin
    phpmyadmin(mysql manager ui based): http://$SERVER_HOSTNAME:1088
    phpredisadmin(redis web ui interface): http://$SERVER_HOST:10010
    emqx(mqtt-server) dashboard: http://$SERVER_HOSTNAME:7083
"
