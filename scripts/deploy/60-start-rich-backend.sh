#!/bin/bash

echo "
# for dev
# fs inotify
sudo sysctl fs.inotify.max_user_watches=524288

# work-around network issue
yarn config set sass-binary-site http://npm.taobao.org/mirrors/node-sass

cd $PROJECT_ROOTDIR/rich_backend
yarn
yarn dev

cd $PROJECT_ROOTDIR/rich_frontend
yarn
yarn serve 
"

# for dev
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

# rich_backend
cd $PROJECT_ROOTDIR/rich_backend
yarn
yarn build
yarn stop
#yarn start
yarn daemon
