#!/bin/bash

for dir in `ls $PROJECT_ROOTDIR`
do
    cd $PROJECT_ROOTDIR/$dir; git reset --hard HEAD; cd -
done
