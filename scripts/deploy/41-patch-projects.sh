#!/bin/bash

# schedule config file
cp patchs/schedule.json $PROJECT_ROOTDIR/deploy-ruichen-docker/schedule.json

# a couple of log file
touch $PROJECT_ROOTDIR/deploy-ruichen-docker/{schedule.log,app.log}

# generate config.json from template, the main reason is that the host must be properly configured
sed -e 's/$SERVER_HOSTNAME/127.0.0.1/' < patchs/deploy-ruichen-docker_config.json.template > $PROJECT_ROOTDIR/deploy-ruichen-docker/config.json

# TODO: public SERVER_HOSTNAME is needed
# generate config.json from template
sed -e 's/$SERVER_HOSTNAME/127.0.0.1/' < patchs/openiot.yunplus.io_config.json.template > $PROJECT_ROOTDIR/openiot.yunplus.io/config.json

# change the prot of mqtt server, otherwise it will not work
sed -i 's/7080:8080/7080:8081/' $PROJECT_ROOTDIR/deploy-ruichen-docker/docker-compose.yml

# init.sh patch
target_file=$PROJECT_ROOTDIR/deploy-ruichen-docker/emqx/init.sh
IFS='' read -r -d '' new_str <<"EOF"
curl --basic -u $ID:$SECRET -X POST -H "Content-Type: application/json" -d '{"username":"admin", "password": "123123123"}' "http://localhost:7080/api/v4/auth_username"
EOF
# sed 's#'''$old_str'''#'''$new_str'''#g' test
awk -v sub_text="$new_str" '{if ($1 == "curl") print sub_text; else print $0;}' $target_file > $target_file.new
cp -v $target_file.new $target_file
