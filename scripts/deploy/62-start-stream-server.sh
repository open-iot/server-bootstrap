#!/bin/bash

# http://open.tendi.cn/libs/tendi/tlive/root.zip

unalias cp

# stream server
cd $PROJECT_ROOTDIR/stream-server/pkg
rm -rf root.zip root
cat root.zip.a* > root.zip
unzip root.zip
cp root/* / -rf
rm -rf root.zip root
cat root.zip.a* > root.zip
chmod 777 /opt/live -R

systemctl enable live
systemctl start live

# stream api service
cd $PROJECT_ROOTDIR/stream-server/src
npm install
pm2 start app.js --name stream-server-api

pm2 startup
pm2 save --force
