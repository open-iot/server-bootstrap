#!/bin/bash

mkdir -p $HOME/bin
tar xvf pkgs/gotty_linux_amd64.tar.gz -C $HOME/bin >/dev/null

cat <<EOF > $HOME/bin/websh
#!/bin/bash
TERM=xterm-256color $HOME/bin/gotty -p 7702 --title-format "Websh share - {{ .Command }} ({{ .Hostname }})" tmux -f /dev/null new -A -s websh-share /bin/bash &
TERM=xterm-256color $HOME/bin/gotty -w -p 7701 --title-format "Websh - {{ .Command }} ({{ .Hostname }})" -c test:7890 /bin/bash
EOF

chmod +x $HOME/bin/websh

#gotty -w -p "7701" -c "test:7890" /bin/bash
mkdir -p $HOME/.config/systemd/user/
cat <<EOF > $HOME/.config/systemd/user/websh.service
[Unit]
Description=web tty

[Service]
ExecStart=$HOME/bin/websh
Restart=always
EOF

systemctl --user daemon-reload
systemctl --user restart websh
