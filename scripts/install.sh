#!/bin/sh

# precondition
# check python2
python2 --version 2>&1 > /dev/null
if [ $? -ne 0 ]; then
    echo "make sure python2 was installed"
    echo "CentOS: yum install python2 -y"
    exit 1
fi

# check git
git --version 2>&1 > /dev/null
if [ $? -ne 0 ]; then
    echo "make sure git was installed"
    echo "CentOS: yum install git -y"
    exit 2
fi

REPO_PATH=/tmp/repo
REPO_CMD="python2 $REPO_PATH"
MANIFESTS_URL=https://gitee.com/open-iot/manifests.git

if [ ! -f $REPO_PATH ]; then
    curl https://gitee.com/open-iot/server-bootstrap/raw/master/utils/repo > $REPO_PATH
fi

$REPO_CMD init -u $MANIFESTS_URL -b master
$REPO_CMD sync -j1
$REPO_CMD start master --all

echo repo sync done
