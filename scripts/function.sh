#!/bin/bash

unrepeat_path()
{
    awk -F: '{
    sep = ""
    for (i = 1; i <= NF; ++i)
        if (unique[$i] != 1) {
            out = out sep $i
            sep = ":"
            unique[$i] = 1
        }
        print out
    }' <<< $PATH
}

echo_exec()
{
    echo $*
    $*
}

assert_failed()
{
    echo assert: $1 && exit 0
}

# is aliyun host?
is_alicloud_server()
{
    if [ "`cat /etc/motd | grep 'Alibaba Cloud'`" ]; then
        return 0
    fi
    return 1
}

# $0 describe string
assert_alicloud()
{
    is_alicloud_server
    [ $? -eq 0 ] && assert_failed "$1"
}

export -f unrepeat_path echo_exec
export -f assert_failed
export -f is_alicloud_server assert_alicloud
export PRODUCT_PROFILE=$HOME/bin/product.profile
